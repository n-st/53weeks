## 2024 nach Wochenenden (Fr-So)
- 5.-7.1. (_Mo1_, Sa6)
- 12.-14.1.
- 19.-21.1.
- 26.-28.1.
- 2.-4.2.
- 9.-11.2.
- 16.-18.2.
- 23.-25.2.
- 1.-3.3.
- 8.-10.3.
- 15.-17.3.
    - 15.-16.3. Mammut Metal-Festival, Augsburg #metal
- 22.-24.3.
- **29.-31.3.** (Fr29)
    - **29.3.-1.4. Easterhegg, Regensburg** #ccc
- **5.-7.4.** (_Mo1_)
    - **4.-6.4. Ragnarök, Lichtenfels** #metal  
      (116€)
- 12.-14.4.
- 19.-21.4.
    - 19.-20.4. Metal Franconia Festival, Geiselwind #metal  
      (62€/72€)
- 26.-28.4.
    - 26.-28.4. UnFUCK, Furtwangen #ccc
- **3.-5.5.** (_Mi1_)
    - **3.-5.5. FSCK, Backnang** #ccc
- **10.-12.5.** (_Do9_)
    - **9.-11.5. Darktroll, Schweinsburg Bornstedt** #metal  
      (114€)
- 17.-19.5.
    - 16.-19.5. Camping Battleground Festival, Öttingen #metal  
      (79€/)  
      with Gutalax and Abbie Falls
- 24.-26.5. (_Mo20_)
- **31.5.-2.6.** (_Do30_)
    - **30.5.-2.6. GPN, Karlsruhe** #ccc  
      (0€/40€)
    - ~~31.5.-1.6. Bavarian Battle Open Air, Kirchdorf am Inn~~ #metal  
      (70€/)
    - ~~31.5.-2.6. Metalfest, Plzeň, CZ~~ #metal  
      (95€)
- 7.-9.6.
    - 6.-8.6. Rock am Berg, Merkers #bunt  
      (95€)
    - 6.-9.6. IGERla, Bamberg #ccc
    - ~~6.-8.6. Wasted Open Air, Obernzenn~~ #bunt
- **14.-16.6.**
    - **13.-16.6. VVoidCamp, Perlesreut** #ccc  
      (>= 65€)
- 21.-23.6.
    - 21.-23.6. Full Force, Gräfenhainichen #metal  
      (170€)
    - 20.-22.6. Aaargh Festival, tbd im Allgäu #metal  
      (>= 66€)
    - 21.-23.6. Kilkim Žaibu, Gražai, LT #metal
    - 21.-22.6. Boarstream, Buchenbach #metal
- **28.-30.6.**
    - **26.-30.6. Fusion, Lärz/Mecklenburg** #electro  
      (>= 220€)
    - 28.-29.6. Rock am Härtsfeldsee, Dischingen in Ostwürttemberg #metal  
      (42€)
    - 27.-29.6. Hörnerfest, Hörnerkirchen bei Elmshorn #metal  
      (75€)
    - 26.-29.6. Basinfirefest, Spálene Pořiči, CZ #metal  
      (82€)
- 5.-7.7.
    - 3.-7.7. Obscene Extreme, Trutnov, CZ #metal  
      (155€)
    - 3.-6.7. Rockharz, Ballenstedt #metal  
      (175€)
    - 4.-6.7. Electric Love, Salzburg #electro
    - 5.-6.7. Schicht im Schacht, Merkers #metal
- 12.-14.7.
    - 12.-13.7. Pure Fucking Metal FestEvil, Hart/Laberweinting #metal  
      (55€)
    - ~~11.-13.7. Dong Open Air, Neukirchen-Vlyun bei Duisburg~~ #metal  
      (>= 125€)
    - 11.-13.7. Field Invasion, Urbach #metal  
      (/38€)
    - ~~9.-13.7. Hacken Open Air, Gifhorn~~ #ccc  
      (>= 185€)
- **19.-21.7.**
    - **19.-20.7. Krach am Bach, Prölsdorf** #bunt
    - 17.-24.7. Bornhack, Funen, DK #ccc  
      (89€/180€)
    - ~~19.-20.7. Black Mountain, Schwarzenberg~~ #gothic
    - ~~19.-20.7. Baden in Blut, Weil am Rhein~~ #metal  
      (>= 95€)
- **26.-28.7.**
    - 24.-28.7. Tolminator, Tolmin, SI #metal  
      (150€)
    - **26.-28.7. Schlichtenfest, Ottobeuren** #metal  
      (>= 56€)
- **2.-4.8.**
    - **29.7.-4.8. Wacken Open Air, Wacken** #metal  
      (333€)  
      Sonntagsanreise möglich…
    - 2.-4.8. Open Air Gößnitz, Gößnitz #metal  
      (~70€)
    - ~~1.-3.8. Metal United Festival, Regensburg~~ #metal  
      (39€/)
    - ~~2.-4.8. Sticky Fingers, Marktredwitz~~ #metal  
      (38€)
- 9.-11.8.
    - 7.-10.8. Brutal Assault, Jaroměř, CZ #metal  
      (175€)
    - 8.-10.8. Party.San, Schlotheim (Thüringen) #metal  
      (136€)
- 16.-18.8. (_Do15_)
    - 14.-18.8. Summer Breeze, Dinkelsbühl #metal  
      (200€/240€)
    - 17.-18.8. FrOSCon, Sankt Augustin #example
- 23.-25.8.
    - 22.-25.8. Westwood Camp, Rotenhain bei Montabaur #ccc
- 30.8.-1.9.
    - 28.8.-1.9. Håck ma's Castle, Ottenschlag, AT #ccc
- 6.-8.9.
    - 6.-7.9. Hellseatic Open Air, ~ Bremen #metal  
      (70€)
- 13.-15.9.
- **20.-22.9.**
    - **20.-21.9. Metal im Woid, Schrobenhausen** #metal  
      (59€)
    - 20.-22.9. Datenspuren, Dresden #ccc
- 27.-29.9.
    - 28.9. Crewsade of Metal, Pfaffenhofen/Ilm #metal
- **4.-6.10.** (_Do3_)
    - **3.-6.10. MRMCD, Darmstadt** #ccc  
      (~42€)
- 11.-13.10.
- 18.-20.10.
- 25.-27.10.
    - 25.-26.10. Mammut Metal-Festival, Augsburg #metal
- 1.-3.11. (Fr1)
    - 31.10.-3.11. DHCP, Heilbronn #ccc
- 8.-10.11.
- 15.-17.11.
- 22.-24.11.
- 29.11.-1.12.
- 6.-8.12.
- 13.-15.12.
- 20.-22.12.
- **27.-29.12.** (_Mi25_, _Do26_)
    - **27.-30.12. 38C3, Hamburg** #ccc
- 3.-5.1.
