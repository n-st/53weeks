#!/usr/bin/env python
# coding: utf-8

# Define category colors here:
categories = {
    'bunt': '#d3a967',
    'ccc': '#ddcb55',
    'electro': '#6ea68f',
    'metal': '#8855a8',
    'personal': '#3794ac',
}

