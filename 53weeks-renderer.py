#!/usr/bin/env python
# coding: utf-8

# In[ ]:


year = 2024


# In[ ]:


try:
	from categories import categories
except:
	categories = []

for category in categories:
    rgb = categories[category]
    if rgb.startswith('#') and len(rgb) == 7:
        r, g, b = int(rgb[1:3], 16), int(rgb[3:5], 16), int(rgb[5:7], 16)
        categories[category] = 'rgba(%d, %d, %d, 0.5)' % (r, g, b)


# The template will need:
# - a list of all weeks in the year, containing
#   - `month`: the month name/names the week is in
#   - `days`: a list of all days in the week, containing
#     - `weekday`: weekday
#     - `weenend`: is a weekend day (Saturday/Sunday)
#     - `bank_holiday`: is bank holiday
#     - `nonworking_day`: is nonworking day
#   - `events`: a list of all events in the week (reduced to the part lying within the week), containing
#     - `startdate`: start date
#     - `enddate`: end date
#     - `start_offset`: start day (offset from week's start)
#     - `length` (in days)
#     - `summary`: event title
#     - `description`: event description text (if any)
#     - `location`
#     - `category`: CSS class (for colour)

# In[ ]:


import datetime
import uuid
import icalendar
import hashlib
import sys
import os.path
import re

event_calendar = []
used_categories = []
for path_to_ics_file in sys.argv[1:]:
    with open(path_to_ics_file) as f:
        calendar = icalendar.Calendar.from_ical(f.read())
    for event in calendar.walk('VEVENT'):
        try:
            startdate = event.decoded("DTSTART")
            if type(startdate) is datetime.datetime:
                startdate = startdate.date()

            if not event.get("DTEND"):
                enddate = startdate
            else:
                enddate = event.decoded("DTEND")
                if type(enddate) is datetime.date:
                    # note that according to https://tools.ietf.org/html/rfc5545, the DTEND is not part of the event period, so we subtract 1 day
                    enddate -= datetime.timedelta(days=1)
                elif type(enddate) is datetime.datetime:
                    # if DTEND contains a time (not just a date), then that day is part of the event, so no -1day here
                    # only convert from datetime to date
                    enddate = enddate.date()

            if event.get("CATEGORIES"):
                category = event.get("CATEGORIES").to_ical().decode('utf-8')
            else:
                # get filename of current input file
                filename, fileext = os.path.splitext(os.path.basename(path_to_ics_file))
                # replace all non-alnum characters
                category = re.sub(r'[^A-Za-z0-9_]', r'-', filename).lower()
                # CSS class names cannot start with a digit
                category = re.sub(r'^([0-9])', r'_\1', category)

            if event.get("UID"):
                uuidclass_components = event.get("UID")
            else:
                uuidclass_components = '%s|%s' % (startdate, enddate)
                uuidclass_components += '|%s' % (event.get("SUMMARY"))
                if category:
                    uuidclass_components += '|%s' % (category)

            uuidclass = 'event-%s' % hashlib.sha256(uuidclass_components.encode('utf8')).hexdigest()

            element = (
                startdate,
                enddate,
                event.decoded("SUMMARY").decode('utf-8'),
                event.decoded("DESCRIPTION").decode('utf-8') if event.get("DESCRIPTION") else '',
                event.decoded("LOCATION").decode('utf-8') if event.get("LOCATION") else '',
                category,
                event.get("STATUS").to_ical().decode('utf-8') if event.get("STATUS") else '',
                uuidclass,
            )
            event_calendar.append(element)
            used_categories.append(category)
        except Exception as e:
            print('Error while processing "%s" (%s): %s' % (path_to_ics_file, event, repr(e)))

for unused_category in set(categories) - set(used_categories):
    del categories[unused_category]

if not event_calendar:
    print('No events were loaded. Rendering an empty calendar.')

holidays = []
try:
    with open('holidays.ics') as f:
        calendar = icalendar.Calendar.from_ical(f.read())
        holidays = [event.decoded("DTSTART") for event in calendar.walk('VEVENT')]
except FileNotFoundError:
    # No holidays then. *shrug*
    print('No holidays.ics found, thus not marking any bank holidays in calendar.')
    pass


# In[ ]:


def contained_events(monday, sunday):
    debug = False
    
    def in_week(date):
        return date >= monday and date <= sunday
        
    result = []
    for event in event_calendar:
        # event completely contained in week:
        if in_week(event[0]) and in_week(event[1]):
            if debug:
                print(event[3], 'contained in', monday, sunday)
            start_offset = (event[0] - monday).days
            length = (event[1] - event[0]).days + 1
            
        # event starts in week (+ ends after it):
        elif in_week(event[0]) and not in_week(event[1]):
            if debug:
                print(event[3], 'starts in', monday, sunday)
            start_offset = (event[0] - monday).days
            length = (sunday - event[0]).days + 1
            
        # event ends in week (+ starts before it):
        elif not in_week(event[0]) and in_week(event[1]):
            if debug:
                print(event[3], 'ends in', monday, sunday)
            start_offset = 0
            length = (event[1] - monday).days + 1
            
        # event encompasses week:
        elif event[0] < monday and event[1] > sunday:
            if debug:
                print(event[3], 'encompasses', monday, sunday)
            start_offset = 0
            length = 7 # entire week

        # event does not concern week at all
        else:
            continue

        result.append(
            {
                'start_offset': start_offset,
                'length': length,
                'startdate': event[0],
                'enddate': event[1],
                'summary': event[2],
                'description': event[3],
                'location': event[4],
                'category': event[5],
                'status': event[6],
                'uuidclass': event[7],
            }
        )

    return result


# In[ ]:


import datetime

# we want to cover full weeks, even if they partially fall outside the selected year
first_isodayofyear = datetime.date(year=year, month=1, day=1).isocalendar()
last_isodayofyear = datetime.date(year=year, month=12, day=31).isocalendar()
first_monday = datetime.date.fromisocalendar(first_isodayofyear.year, first_isodayofyear.week, 1)  # (year, week, day of week)
last_sunday = datetime.date.fromisocalendar(last_isodayofyear.year, last_isodayofyear.week, 7)  # (year, week, day of week)

start = first_monday
end = last_sunday

weeks = []

for weekdelta in range(0, (end - start).days + 1, 7):
    week = {}
    week['days'] = []
    for daydelta in range(7):
        result_date = start + datetime.timedelta(days=weekdelta+daydelta)
        day = {
            'date': result_date,
            'number': result_date.day,
            'weekday': result_date.isoweekday(),
            'weekend': result_date.isoweekday() in [6, 7],
            'bank_holiday': result_date in holidays,
            'nonworking_day': result_date.isoweekday() in [5],
        }
        week['days'].append(day)

    # monthname formatting
    monday = week['days'][0]['date']
    sunday = week['days'][-1]['date']
    month_monday = monday.strftime('%b')
    month_sunday = sunday.strftime('%b')
    week['month'] = month_monday
    if month_sunday != month_monday:
        week['month'] += '/' + month_sunday
    
    week['events'] = contained_events(monday, sunday)
    
    weeks.append(week)


# In[ ]:


import jinja2

templateLoader = jinja2.FileSystemLoader(searchpath="./")
templateEnv = jinja2.Environment(loader=templateLoader)
TEMPLATE_FILE = "calendar.html.j2"
template = templateEnv.get_template(TEMPLATE_FILE)
outputText = template.render(year=year, weeks=weeks, categories=categories, day_width='6em')  # this is where to put args to the template renderer

with open('rendered.html', 'w') as f:
    f.write(outputText)

from IPython.display import display, HTML
display(HTML(outputText))


# In[ ]:


import jinja2

templateLoader = jinja2.FileSystemLoader(searchpath="./")
templateEnv = jinja2.Environment(loader=templateLoader)
TEMPLATE_FILE = "calendar.md.j2"
template = templateEnv.get_template(TEMPLATE_FILE)
outputText = template.render(year=year, weeks=weeks, categories=categories, day_width='6em')  # this is where to put args to the template renderer

with open('rendered.md', 'w') as f:
    f.write(outputText)

from IPython.display import display, Markdown
display(Markdown(outputText))
#print(outputText)

