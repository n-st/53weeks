#!/usr/bin/env python3
# encoding: utf-8 (as per PEP 263)

from datetime import datetime, timedelta
import fileinput
import pytz
import re
import sys
try:
    from icalendar import Calendar, Event
except:
    sys.stderr.write('Failed to import icalendar. Please install python-icalendar and try again.\n')
    sys.exit(1)

def main():
    cal = Calendar()
    cal.add('prodid', '-//Markdown-to-iCal//voidptr.de//')
    cal.add('version', '0.1')

    previous_event = None

    # grab all events
    # split into date / summary / location / category / description
    # grab confirmation status
    for line in fileinput.input(encoding="utf-8"):
        line = line.rstrip('\n')
        event_startline = re.compile(r"""
            ^\s*-[ ]        # start of line = Markdown list item
            ([*~]{1,2})?    # 1=bold/strikethrough, if any
            (\d+\.)(\d+\.)? # 2+3=start date, e.g. 12. or 12.3.
            (
            -
            (\d+\.)(\d+\.)  # 5+6=end date and month, e.g. 14.3.
            )?              # event may be single-day, and thus missing the -xx.yy. end date
            [ ]
            ([^(].+?)       # 7=event title (if it starts with "(", this is not an event line, but a week line with list of holidays)
            (,[ ](.+?))?    # (8+)9=event location (optional), made nongreedy so it doesn't eat the #category
            ([*~]{1,2})?    # 10=bold/strikethrough, if any
            ([ ][✓✗»])?     # 11=personal decision marker, if any
            ([ ][#](\w+))?  # (12+)13=event category, e.g. #metal
            \s*             # there might be whitespace after the category tag
            $
            """, re.VERBOSE)
        event_contdline = re.compile(r"""
            ^\s*            # however-much space (lines might be generally indented)
            [ ]{2}          # ... but they need to at least be indented below a list item (2 spaces)
            ([^\s-].*?)\s*$       # and then capture any line contents
            """, re.VERBOSE)

        if m := event_startline.fullmatch(line):
            status, start_day, start_month, _, end_day, end_month, summary, _, location, _, _, _, category = m.groups()

            if status and status.startswith('*'):
                status = 'CONFIRMED'
            elif status and status.startswith('~'):
                status = 'CANCELLED'
            else:
                status = None

            if not end_day and not end_month:
                end_day = start_day
                end_month = start_month
            elif not start_month:
                start_month = end_month

            start_day, start_month, end_day, end_month = [int(d.rstrip('.')) for d in [start_day, start_month, end_day, end_month]]

            event = Event()
            event.add('summary', summary)
            event.add('dtstart', datetime(2024, start_month, start_day).date())
            # end date is exclusive (not part of event duration), so add +1day
            event.add('dtend', datetime(2024, end_month, end_day).date() + timedelta(days=1))
            event.add('dtstamp', datetime(2024, start_month, start_day, 0, 0, 0, tzinfo=pytz.utc))

            if location:
                event.add('location', location)
            if status:
                event.add('status', status)
            if category:
                event.add('categories', category)

            cal.add_component(event)

            # remember last-seen event so we can tack on any follow-up (description) lines
            previous_event = event

        if m := event_contdline.fullmatch(line):
            content, = m.groups() # 1:1 variable mapping, error out if we have more/fewer match groups than expected
            if previous_event:
                if 'description' in previous_event:
                    content = previous_event['description'] + '\n' + content
                previous_event['description'] = content
            else:
                sys.stderr.write('WARNING: Found a follow-up line without a preceding event line:')
                sys.stderr.write(content)

    print(cal.to_ical().decode("utf-8"))


if __name__ == '__main__':
    main()

