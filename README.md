53weeks calendar generator
==========================

This collection of scripts can generate calendars in the form of

- (static but Javascript-enriched) HTML
- Markdown lists

It can read calendar events and public holidays from iCalendar files.

Its main unique feature is that it shows an entire year as a list of weeks — hence "53weeks"(*).

(* Yes, there are cases where a calendar year actually "touches" 54 weeks. 53weeks will work with those as well.)

There is also a script (`md-to-ical.py`) that provides the round-trip conversion from Markdown into iCalendar format.

## Examples

There is a pre-commit hook that (if you installed it) automatically generates the HTML and Markdown versions of the `example.ics` calendar included with this repo.  
(Incidentally, the `example.ics` was generated using the round-trip converter `md-to-ical.py` from this repo.)

Have a look at the [HTML example](example.html) and [Markdown example](example.md) to see what sort of result to current version of 53weeks produces.

## Requirements

- Python 3
- python-icalendar
- python-jinja

## Development/Usage/Caveats

This project mainly serves a personal requirement for a full-year week calendar for easier pre-planning.  
Its development began with experiments and proofs-of-concept in a Jupyter Notebook, with most of the paths and calendar events hardcoded for simplicity.  
I have retroactively converted the Jupyter Notebook into plain Python for a clear Git history, and started moving to a more configurable interface, but **if you want to use this for yourself, you will probably still need to modify the code in several places**.
